from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient

# Register your models here.
# admin.site.register(Recipe)


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "recipe",
        "id",
        )


@admin.register(Ingredient)
class IngredientsAdmin(admin.ModelAdmin):
    list_display = (
        "food_item",
        "amount",
        "recipe",
        "id",
        )
