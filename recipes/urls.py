from django.urls import path
from recipes.views import recipe_list, show_recipes, create_recipe, edit_recipe

urlpatterns = [
    path("", recipe_list, name="main_home"),
    path("<int:id>/", show_recipes, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
]
